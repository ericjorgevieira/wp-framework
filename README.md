# Framework Wordpress

Wordpress framework stack built on top of [bedrock](http://roots.io/bedrock) and [sage](http://roots.io/sage).

## Requirements

- PHP >+ 5.5
- Composer
- Ruby >+ 2.0
- Bundler
- NodeJS and NPM

## Included plugins

Not all plugins are required.

- `wpackagist-plugin/types:1.9.1` - helps creating custom post types;
- `wpackagist-plugin/wordpress-seo:3.1.1` - for search engine optimization;
- `wpackagist-plugin/contact-form-7:4.4` - creates easy contact forms;
- `wpackagist-plugin/jetpack:3.9.4` - simplifies managing wordpress sites;
- `wpackagist-plugin/w3-total-cache:0.9.4.1` - easy caching;
- `wpackagist-plugin/advanced-custom-fields:4.4.5` - creating custom fields.

When you start a new project you must remove the plugins that won't be used from the `src/composer.json` file.

## Creating a new project

1. Clone the git repository - `git clone git@bitbucket.org:mobister/wordpress-framework.git [project_name]`
2. Enter project folder - `cd [project_name]`
3. Remove git repository and create a new one - `rm -rf .git` `git init`
4. Create a new repository on bitbucket and add to your local repository - `git remote add origin [repository_url]`
5. Commit the project initial state - `git add --all` `git commit -m "Initial commit"` `git push origin master`

## Development environment

1. Enter the source folder - `cd src`
2. Install composer dependencies - `composer install` (avoid update)
3. Create a configuration file - `cp .env.example .env` (do not remove the example file)
4. Set the environment variables in the `.env` file
5. Configure your webservice or use PHP Builtin Server*

\* Run `php mobister serve {--host=localhost} {--port=8000}`

## Staging enviroment

1. Configure a webhook in the bitbucket repository (use any recent repository as a example)
2. Push any changes to the repository

The staging server will run some processes, and then the url `http://[repository_name].local.mobister.com.br`. It will probably show some error, it means the server probably didn't run the `composer install` command. You should run, or ask someone to run it manually via SSH.

## Production server

### First configuration

1. Run `bundle install` inside `src` directory to install gem dependencies
2. Configure application name, repository url in `src/config/deploy.rb`
3. Configure production server url in `src/config/environment/production.rb`
4. Run `bundle exec cap production deploy:check`

The command will failed because it won't find the shared files. To fix this, you should connect to the server and create the shared files manually. The list of linked files you can found in the `src/config/deploy.rb` file. For now, there are only two files:

1. Enter the shared folder - `cd /sites/[application_name]/shared`
2. Create both files - `touch src/.env src/web/.htaccess`

### Deploy

Obviously you should have a virtual host pointing to `/sites/[application_name]/current/web`. Then, only one command is required to deploy your latest changes: `bundle exec cap production deploy`.

## Known bugs

1. The staging server does not run `composer install` automatically;
2. The staging server does not run `bower install` automatically;
3. The staging server does not run `gulp` automatically.

## Roadmap

1. Integrate bedrock with capistrano;
2. Fixes the problem with the staging server.