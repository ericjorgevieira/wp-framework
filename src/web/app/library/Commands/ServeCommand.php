<?php

namespace Mobister\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\ProcessUtils;
use Symfony\Component\Process\PhpExecutableFinder;

class ServeCommand extends Command
{
    /**
     * Configures command.
     */
    protected function configure()
    {
        $this
            ->setName('serve')
            ->setDescription('Serve the application on the PHP development server')
            ->addOption(
                'host',
                null,
                InputOption::VALUE_OPTIONAL,
                'The host address to serve the application on.',
                'localhost'
            )
            ->addOption(
                'port',
                null,
                InputOption::VALUE_OPTIONAL,
                'The port to serve the application on.',
                8000
            );
    }

    /**
     * Executes command to start a development server.
     *
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $host = $input->getOption('host');
        $port = $input->getOption('port');

        $base = ProcessUtils::escapeArgument(realpath(__DIR__.'/../../../../public'));
        $binary = ProcessUtils::escapeArgument((new PhpExecutableFinder())->find(false));

        $output->writeln("<info>PHP development server started on http://{$host}:{$port}</info>");
        passthru("{$binary} -S {$host}:{$port} -t {$base}");
    }
}
